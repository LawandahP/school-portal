FROM node:14.17.5

WORKDIR /app

COPY . .
COPY package.json ./
#RUN apk add yarn
RUN yarn install --production
RUN yarn build
RUN yarn global add serve

# start app
CMD serve -s build -l 3000