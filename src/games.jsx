/* eslint-disable no-multi-str */
const games = [
    {
        "name": "Chess",
        "image": "/images/chess.jpg",
        "exerpt": "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Suscipit assumenda repellendus voluptate, expedita nemo, exercitationem dolores eius totam sapiente.",
        "teacher": "Principal"
    },
    {
        "name": "Football",
        "image": "/images/football.jpg",
        "exerpt": "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Suscipit assumenda repellendus voluptate, expedita nemo, exercitationem dolores eius totam sapiente.",
        "teacher": "Principal"
    },
    {
        "name": "BasketBall",
        "image": "/images/basketball.jpg",
        "exerpt": "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Suscipit assumenda repellendus voluptate, expedita nemo, exercitationem dolores eius totam sapiente.",
        "teacher": "Principal"
    },
    {
        "name": "Hockey",
        "image": "/images/hockey.jpg",
        "exerpt": "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Suscipit assumenda repellendus voluptate, expedita nemo, exercitationem dolores eius totam sapiente.",
        "teacher": "Principal"
    },
    {
        "name": "Rugby",
        "image": "/images/rugby.jpg",
        "exerpt": "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Suscipit assumenda repellendus voluptate, expedita nemo, exercitationem dolores eius totam sapiente.",
        "teacher": "Principal"
    },
    {
        "name": "Table Tennis",
        "image": "/images/tabletennis.jpg",
        "exerpt": "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Suscipit assumenda repellendus voluptate, expedita nemo, exercitationem dolores eius totam sapiente.",
        "teacher": "Principal"
    },
    {
        "name": "Bad Minton",
        "image": "/images/badminton.jpg",
        "exerpt": "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Suscipit assumenda repellendus voluptate, expedita nemo, exercitationem dolores eius totam sapiente.",
        "teacher": "Principal"
    },
    {
        "name": "Volley Ball",
        "image": "/images/volleyball.jpg",
        "exerpt": "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Suscipit assumenda repellendus voluptate, expedita nemo, exercitationem dolores eius totam sapiente.",
        "teacher": "Principal"
    },
    
   
]

export default games