
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import HomeScreen from './screens/HomeScreen';
import LoginScreen from './screens/LoginScreen';
import { Suspense } from 'react';
import { ThemeProvider, createTheme } from '@mui/material/styles';


const theme = createTheme({
  palette: {
    primary: {
      main: "#333996",
      light: '#3c44b126'
    },
    secondary: {
      main: "#f83245",
      light: '#f8324526'
    },
    background: {
      default: "#f4f5fd"
    },
  },
  overrides:{
    MuiAppBar:{
      root:{
        transform:'translateZ(0)'
      }
    }
  },
  props:{
    MuiIconButton:{
      disableRipple:true
    },
    MuiListItem: {
      disablePadding: true
    }
  }
})


function App() {
    return (

        <Suspense fallback="loading..">
            <Router>
                <Route path='/' component={HomeScreen} exact/>
                <Route path='/login' component={LoginScreen} exact/>
            </Router>
        </Suspense>

    );
}

export default App;
