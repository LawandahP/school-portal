import React from 'react'

import { useTranslation } from 'react-i18next'

import Header from '../components/Header'
import HomeCarousel from '../components/HomeCarousel';
import Testimonials from '../testimonials/Testimonial';
import ContactInfo from '../contacts/ContactInfo';
import SchoolHistory from '../schoolHistory/SchoolHistory';

import { Row, Col, Container } from 'react-bootstrap';

import GooglePlaces from '../components/Map';
import OurTeachers from '../components/OurTeachers';
import Activities from '../components/Activities';


function HomeScreen() {

    const { t } = useTranslation();

    return (
        <div>
            <Header />
            {/* <NavigationBar className="mb-5" /> */}
            <HomeCarousel />
            

            <section className="school-motto">
                <Container>
                    <h2>{ t("motto") }</h2>
                </Container>
            </section>

            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                <path fill="#00D3FF" fill-opacity="1" d="M0,32L40,58.7C80,85,160,139,240,176C320,213,400,235,480,218.7C560,203,640,149,720,144C800,139,880,181,960,197.3C1040,213,1120,203,1200,170.7C1280,139,1360,85,1400,58.7L1440,32L1440,320L1400,320C1360,320,1280,320,1200,320C1120,320,1040,320,960,320C880,320,800,320,720,320C640,320,560,320,480,320C400,320,320,320,240,320C160,320,80,320,40,320L0,320Z"></path>
            </svg>

            <section className="school-vision">
                <Container>
                    
                    <Row>
                        <Col md={4}>
                            <Row>
                                <Col>
                                <h3>SCHOOL MISSION</h3>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                <p>To be a World-Class centre
                                of Academic, Spiritual and
                                Moral Excellence.
                                </p>
                                </Col>
                            </Row>
                        </Col>
                        <Col md={4}>
                            <Row>
                                <Col>
                                <h3>SCHOOL VISION</h3>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <p>To be a World-Class centre
                                    of Academic, Spiritual and
                                    Moral Excellence.
                                    </p>
                                </Col>
                            </Row>
                        </Col>
                        <Col md={4}>
                            <Row>
                                <Col>
                                <h3>CORE VALUES</h3>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <p>To be a World-Class centre
                                        of Academic, Spiritual and
                                        Moral Excellence.
                                    </p>
                                </Col>
                            </Row>
                            
                        </Col>

                    </Row>
                    
                    
                </Container>
            </section>

                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                    <path fill="#00D3FF" fill-opacity="1" d="M0,32L40,58.7C80,85,160,139,240,176C320,213,400,235,480,218.7C560,203,640,149,720,144C800,139,880,181,960,197.3C1040,213,1120,203,1200,170.7C1280,139,1360,85,1400,58.7L1440,32L1440,0L1400,0C1360,0,1280,0,1200,0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"></path>
                </svg>

            <SchoolHistory />
            
            

            <section>
                <Testimonials />
            </section>
            

            {/* <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                    <path fill="#00D3FF" fill-opacity="1" d="M0,32L40,58.7C80,85,160,139,240,176C320,213,400,235,480,218.7C560,203,640,149,720,144C800,139,880,181,960,197.3C1040,213,1120,203,1200,170.7C1280,139,1360,85,1400,58.7L1440,32L1440,0L1400,0C1360,0,1280,0,1200,0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"></path>
                </svg> */}
            <OurTeachers />
            <Activities />
            <ContactInfo />
            <GooglePlaces />

            
            

            
        </div>
    )
}

export default HomeScreen
