import React, { useState } from 'react'

import { Form, Button, InputGroup, Container, Row, Col} from 'react-bootstrap'

function LoginScreen() {

    const [isReaveled, setIsRevealed] = useState(false)

    return (
        
        <div>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#0099ff" fill-opacity="1" d="M0,96L40,128C80,160,160,224,240,218.7C320,213,400,139,480,128C560,117,640,171,720,176C800,181,880,139,960,133.3C1040,128,1120,160,1200,154.7C1280,149,1360,107,1400,85.3L1440,64L1440,0L1400,0C1360,0,1280,0,1200,0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"></path></svg>

       
        
        
        <Container>
            <Row>
                <Col md={4}>
                    <Form>
                        <Form.Group controlId='email'>
                            <Form.Label>Email Address</Form.Label>
                            <Form.Control 
                                type='email'
                                placeholder='Enter Email'
                                >
                            </Form.Control>
                        </Form.Group>

                        <Form.Group controlId='password' className='my-2'>
                                    
                            <Form.Label>Password</Form.Label>
                                <InputGroup className="mb-3">
                                    <Form.Control 
                                        required
                                        type={isReaveled ? "text" : "password"}
                                        placeholder='Enter Password'
                                        >
                                    </Form.Control>

                                <InputGroup.Text id="hide-show-pwd" 
                                    title={isReaveled ? "Hide password" : "Show Password"}
                                    onClick={() => setIsRevealed(prevState => !prevState)}>
                                    {isReaveled ? 
                                        <i className='fas fa-eye-slash'></i> : <i className='fas fa-eye'></i>}
                                </InputGroup.Text>

                            </InputGroup>                          
                        </Form.Group>

                        <div className="d-grid gap-2 my-2" align="center">
                            <Button type='submit' align="center" className='d-grid gap-2 my-3'>
                                Sign In
                            </Button>
                        </div>

                    </Form>
                </Col>
            </Row>
            
        </Container>

        </div>

    )
}

export default LoginScreen
