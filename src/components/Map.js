// import React, {useEffect} from 'react';
// import { useDispatch, useSelector } from 'react-redux';

// import { GoogleMap } from "react-google-maps";
// import { getContact } from '../actions/contactActions';


// export default function Map() {

//     const dispatch = useDispatch();
//     const contactList = useSelector(state => state.contactList);
//     const { error, loading, contact } = contactList

//     useEffect(() => {
//         dispatch(getContact())
//     }, [dispatch])

//     return (
//         <GoogleMap
//             defaultZoom={10}
//             defaultCenter={{lat: -1.292066 , lng: 36.821945}}>
            
//         </GoogleMap>
//     )
// }


import React, { Component } from 'react';
import {Map, Marker, InfoWindow, GoogleApiWrapper} from 'google-maps-react';


export class GooglePlaces extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // for google map places autocomplete
      address: '',
      //copy to clipboard
      value: '',
      copied: false,

      showingInfoWindow: true,
      activeMarker: {},
      selectedPlace: {},
       
      mapCenter: {
        lat: -1.283043754590014,
        lng: 36.82199703904182
      }
    };
    
    this.onMarkerClick = this.onMarkerClick.bind(this);
    this.onClose = this.onClose.bind(this);
  }

  onMarkerClick = (props, marker, e) => {
    this.setState(prevState => ({
    selectedPlace: props,
    activeMarker: marker,
    showingInfoWindow: true
    }));
  }

  onClose = () => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      });
    }
  };

  onMarkerMounted = element => {
    console.log(element.marker)
  this.onMarkerClick(element.props, element.marker, element);
  };

  

  handleChange = (address) => {
    this.setState({ address });
  };

  render() {
  
    const containerStyle = {
      position: 'relative'  
    }

    return (
      <div id='googleMaps'>
        <div className="pt-2" style={{height: "60vh", width: "100%"}}>
        <Map
          containerStyle={containerStyle}
          google={this.props.google}
          zoom={15}
          initialCenter={{
            lat: this.state.mapCenter.lat,
            lng: this.state.mapCenter.lng
          }}
          center={{
            lat: this.state.mapCenter.lat,
            lng: this.state.mapCenter.lng
          }}
        >
          
          
        <Marker position={{
          lat: this.state.mapCenter.lat,
          lng: this.state.mapCenter.lng}}
          onClick={this.onMarkerClick}
        />
        
        {this.state.address && (
            <InfoWindow
              marker={this.state.activeMarker}
              visible={this.state.showingInfoWindow}
              onClose={this.onClose}
            >
              <div style={{color: '#f16100'}}>{this.state.address}</div>
            </InfoWindow>
        )}

        </Map>
        </div>
      </div>
    )
  }
}

export default GoogleApiWrapper({
  apiKey: ('AIzaSyBRhU21uqPaBFmGft_1b_haS_mEjHRNmDA')
})(GooglePlaces)
