import React from 'react';
import { Container, Nav, Navbar, NavDropdown, Button, Form, InputGroup } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { Link } from 'react-router-dom';
import i18n from '../i18n';


function Header() {

  const toggleLanguage = (event) => {
    i18n.changeLanguage(event.target.value)
  }

  return (
    <div>
      <Navbar variant="" expand="lg">
        <Container>

          <LinkContainer to="/">
            <Navbar.Brand>SKOOLY</Navbar.Brand>
          </LinkContainer>

          <Navbar.Toggle aria-controls="basic-navbar-nav"><i class="fas fa-bars"></i></Navbar.Toggle>
          <Navbar.Collapse id="basic-navbar-nav">

            <Nav>
              <Nav.Link href="#home">Home</Nav.Link>
              <Nav.Link href="#about-us">About Us</Nav.Link>
              <Nav.Link href="#home">Calender</Nav.Link>
              <Nav.Link href="#contact-us">Contact</Nav.Link>
              <NavDropdown title="Dropdown" id="basic-nav-dropdown">
                <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
              </NavDropdown>

            </Nav>

            <InputGroup className="select-language">
              <InputGroup.Text id="basic-addon1"><i className="fas fa-language"></i></InputGroup.Text>
              <Form.Select
                onChange={toggleLanguage}
                >
                <i class="fas fa-language"></i><option value="en"> English</option>
                <i class="fas fa-language"></i><option value="sw"> Swahili</option>
              </Form.Select>
            </InputGroup>
            

            

            <Link to="/login">
                <Button>LOGIN</Button>
            </Link>


          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  )
}

export default Header
