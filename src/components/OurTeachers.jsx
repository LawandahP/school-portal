import { Card, CardActionArea, CardContent, CardMedia, Container, Divider, Grid, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { useTheme } from '@mui/material/styles';
import React from 'react'
import teachers from '../teachers';
import teacherSection from '../assets/teacherSection.jpg'


const useStyles = makeStyles((theme) => ({
    background: {
        backgroundImage: `url(${teacherSection})`,
        opacity: "5000%"
    },
    container: {
        height: "100vh",
        overflow: "auto",
        textAlign: "center",
        paddingBottom: useTheme().spacing(7)
    },
    heading: {
        textAlign: "center",
        position: "sticky",
        color: '#fff',
        paddingTop: useTheme().spacing(5),
        paddingBottom: useTheme().spacing(3)
        
    },
    card: {
        margin: useTheme().spacing(1),
        // height: "20rem"
        // marginBottom: useTheme().spacing(2)
    },
    content: {
        backgroundColor: "#00D3FF",
        color: "#fff"
    },
    
    otherTeachers: {
        width:"100%",
        overflow: "auto",
        // height: "47.2rem",
        // display: "flex"
    },

}));
const OurTeachers = () => {
    const classes = useStyles();

    return (
        
        <div className={classes.background}>
            <Typography className={classes.heading}><h2>Our Staff</h2></Typography>
            <Container className={classes.container}>
            
            {/* <Divider /> */}
                
            
                    <Grid container>
                        {teachers.map(teacher => (
                            <Grid item md={3} xs={12} >
                                
                                    <Card className={classes.card}>
                                        <CardActionArea>
                                            <CardMedia
                                                // className=
                                                component="img"
                                                height="270"
                                                image={teacher.image}
                                                alt={teacher.name}
                                            />
                                            <CardContent className={classes.content}>
                                                <Typography gutterBottom variant="h6" component="div">
                                                    <h5>{teacher.name}</h5>
                                                </Typography>
                                                <Typography variant="body2">
                                                    <p>{teacher.rank}</p>
                                                </Typography>
                                            </CardContent>
                                        </CardActionArea>
                                    </Card>
                            </Grid>
                        ))}
                    </Grid>                    
                         
        </Container>
    
        </div>
            
        
    )
}

export default OurTeachers
