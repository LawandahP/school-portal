import { useTheme } from '@emotion/react';
import { makeStyles } from '@mui/styles';
import React from 'react'
import { Carousel, Image } from 'react-bootstrap'
import image1 from '../assets/image1.jpg';
import image2 from '../assets/image2.jpg';
import image3 from '../assets/image3.jpg';


const useStyles = makeStyles(theme => ({
    container: {
        height: "86vh",
        width: "100%"
        // paddingTop: useTheme().spacing(1),
        // paddingBottom: useTheme().spacing(10),
        // [useTheme().breakpoints.down('sm')]: {
        //     paddingBottom: useTheme().spacing(3),
        // }
        // height: "100vh"
    },
}));

function HomeCarousel() {
    const classes = useStyles();
    return (
        <div>
            <Carousel>
                <Carousel.Item>
                    <Image
                    className={classes.container}
                    src="https://www.mckinsey.com/~/media/mckinsey/careers%20redesign/for%20students/main/careersr2_students_1up-hero-page-header-_1536x864.jpg?mw=1536&car=546:205&cpx=Right&cpy=Center"
                    alt="First slide"
                    />
                    <Carousel.Caption>
                    <h2>Welcome To Gathiini</h2>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <Image
                    className={classes.container}
                    src="https://th.bing.com/th/id/R.4d38aa2d4a4bd844de4212dbc9c3f45a?rik=Ba9sGQGMFNDqtQ&riu=http%3a%2f%2fi.huffpost.com%2fgen%2f5121826%2fimages%2fo-STUDENT-DIVERSE-CLASSROOM-facebook.jpg&ehk=8yir8WWapr412VZ3%2f8ZfE0fg5g0Og8AiCDKUt60iPY0%3d&risl=&pid=ImgRaw&r=0"
                    alt="Second slide"
                    />
                    <Carousel.Caption>
                    <h2>Second slide label</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <Image
                    className={classes.container}
                    src="https://www.mckinsey.com/~/media/mckinsey/careers%20redesign/for%20students/main/careersr2_students_1up-hero-page-header-_1536x864.jpg?mw=1536&car=546:205&cpx=Right&cpy=Center"
                    alt="Third slide"
                    />

                    <Carousel.Caption>
                    <h2>Third slide label</h2>
                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        </div>
    )
}

export default HomeCarousel
