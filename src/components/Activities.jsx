import { Container, Grid, Paper, Typography } from '@mui/material'
import { Card, CardActionArea, CardContent, CardMedia } from '@mui/material';
import React from 'react'

import { makeStyles } from '@mui/styles';
import { useTheme } from '@mui/material/styles';
import games from '../games';


const useStyles = makeStyles((theme) => ({
    background: {
        backgroundColor: "#E9E9E9",
    },
    container: {
        height: "100vh",
        overflow: "auto",
        textAlign: "center",
        // paddingTop: useTheme().spacing(3),
        paddingBottom: useTheme().spacing(3)
    },
    heading: {
        textAlign: "center",
        // color: '#0000',
        paddingBottom: useTheme().spacing(3),
        paddingTop: useTheme().spacing(3)
        
    },
    card: {
        marginLeft: useTheme().spacing(1),
        marginRight: useTheme().spacing(1),
    },

    media: {
        height: "170"
    }
    
    

}));

const Activities = () => {
    const classes = useStyles();

    return (

        <div className={classes.background}>
            <Typography className={classes.heading}><h2>Activities</h2></Typography>
            <Container className={classes.container}>
                <Grid container>
                {games.map(game => (
                    <Grid item md={3} >
                        <Card className={classes.card}>
                            <CardActionArea>
                                <CardMedia
                                    className={classes.media}
                                    component="img"
                                    image={game.image}
                                    alt="image"
                                />
                            </CardActionArea>
                        </Card>
                        <CardContent className={classes.content}>
                            <Typography gutterBottom variant="h5">
                                <h5>{game.name}</h5>
                            </Typography>
                            <Typography variant="body2" color="text.secondary">
                                <p>{game.exerpt}</p>
                            </Typography>   
                        </CardContent>
                    </Grid>  
                ))}      
                </Grid>

            </Container>
        </div>
        
    )
}

export default Activities
