import React, {useState}  from 'react'
import { Toast, Row, Col, ToastContainer } from 'react-bootstrap'


function ToastAlert({bg, children}) {
    const [show, setShow] = useState();
    // const [position, setPosition] = useState('top-end');

  
    return (
      <Row>
        <Col xs={6}>
        <ToastContainer className="p-3" position="top-end">
          <Toast bg={bg} onClose={() => setShow(false)} show={show} delay={5000} autohide>
            <Toast.Header>
              <img
                src=""
                className="me-auto"
                alt=""
              />
              {/* <strong className="me-auto">skooly</strong> */}
              {/* <small>11 mins ago</small> */}
            </Toast.Header>
            <Toast.Body>{children}</Toast.Body>
          </Toast>
        </ToastContainer>
        </Col>
      </Row>
    );
  }

export default ToastAlert