import axios from 'axios'

import { 
    CONTACT_GET_REQUEST,
    CONTACT_GET_SUCCESS,
    CONTACT_GET_FAIL,
} from './contactConstants';

const proxy = process.env.REACT_APP_SAMIS_BACKEND_HOST

export const getContact = () => async (dispatch) => {
    try {
        dispatch({
            type: CONTACT_GET_REQUEST
        })

        const { data } = await axios.get(`${proxy}/contact-info/v1`)

        dispatch({
            type: CONTACT_GET_SUCCESS,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: CONTACT_GET_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}