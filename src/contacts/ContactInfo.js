import React, { useEffect } from 'react';

// import { Container } from 'react-bootstrap';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import { useDispatch, useSelector } from 'react-redux';

import { getContact } from './contactActions'
import Loader from '../components/Loader';
import Notification from '../components/Notification'
import { Card, Typography, CardContent, CardMedia } from '@mui/material';
import { makeStyles } from '@mui/styles'
import { useTheme } from '@emotion/react';

const useStyles = makeStyles(theme => ({
    container: {
        textAlign: "center",
        paddingTop: useTheme().spacing(2),
        paddingBottom: useTheme().spacing(10),
        [useTheme().breakpoints.down('sm')]: {
            paddingBottom: useTheme().spacing(3),
        }
        // height: "100vh"
    },
    card: {
        // alignItems: "center",
        listStyle: "none",
        margin: useTheme().spacing(1)
    },
    heading: {
        textAlign: "center",
        paddingTop: useTheme().spacing(10),
        [useTheme().breakpoints.down('sm')]: {
            paddingTop: useTheme().spacing(3),
        }
    }
}))

function ContactInfo() {
    const classes = useStyles();

    const dispatch = useDispatch();
    const contactList = useSelector(state => state.contactList);
    const { error, loading, contact } = contactList

    useEffect(() => {
        dispatch(getContact())
    }, [dispatch])


    return (
        <div>
            { loading ? <div className="my-6"><Loader /></div> 
            : error ? <Notification variant="danger">{error}</Notification> 
            : (
                <section className="contact-us" id="contact-us">
                    <Typography className={classes.heading}><h2>Contact Us</h2></Typography>
                    <Container className={classes.container}>
                        <Grid container>
                            <Grid item md={4} xs={12} >
                                <Card className={classes.card}>
                                    <CardContent className={classes.content}>
                                        <i class="fas fa-location-arrow ml-5"></i>
                                        <Typography gutterBottom variant="h5">
                                            <h5>Our location</h5>
                                        </Typography>
                                        <Typography variant="body2" color="text.secondary">
                                        {contact.physical_address && contact.physical_address}
                                        <p>{contact.postal_address}</p>
                                        </Typography>   
                                    </CardContent>
                                </Card>
                            </Grid>  

                            <Grid item md={4} xs={12} >
                                <Card className={classes.card}>
                                    <CardContent className={classes.content}>
                                        <i class="fas fa-envelope-open"></i>
                                        <Typography>
                                            <h5>Email</h5>
                                        </Typography>
                                        <Typography variant="body2" color="text.secondary">
                                            {contact.email}
                                        </Typography>
                                        <Typography variant="body2" color="text.secondary">
                                            {/* {contact.other_emails && <h6>Other Emails</h6>} */}
                                
                                            { contact.other_emails && contact.other_emails.map(other_emails => (
                                                <li>{other_emails}</li>
                                            ))}
                                        </Typography>   
                                    </CardContent>
                                </Card>
                            </Grid>  

                            <Grid item md={4} xs={12} >
                                <Card className={classes.card}>
                                    <CardContent className={classes.content}>
                                    <i class="fas fa-phone"></i>
                                        <Typography gutterBottom variant="h5">
                                            <h5>Phone Number</h5>
                                        </Typography>
                                        <Typography variant="body2" color="text.secondary">
                                        {contact.primary_phone_no}
                                        {/* {contact.other_phone_nos && <h6>Other Phone Numbers</h6>} */}
                                        {/* <ul className=''> */}
                                            { contact.other_phone_nos && contact.other_phone_nos.map(phone_nos => (
                                                <li>{phone_nos}</li>
                                            ))}
                                        {/* </ul> */}
                                        </Typography>   
                                    </CardContent>
                                </Card>
                            </Grid>                                  
                        </Grid>
                    </Container>

                </section>
            )}     
                
            
        </div>
    )
}

export default ContactInfo


// physical_address(pin):'Mombasa, Kenya'
// location(pin):{latitude:-1.78898,longitude:1…titude:23}
// postal_address(pin):'string'
// country_code(pin):'+254'
