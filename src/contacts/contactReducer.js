import { 
    CONTACT_GET_REQUEST,
    CONTACT_GET_SUCCESS,
    CONTACT_GET_FAIL
 } from './contactConstants'


 export const contactGetReducer = (state = { contact: {}}, action) => {
    switch(action.type) {
        case CONTACT_GET_REQUEST:
            return {
                loading: true,
                contact: {}
            }
        
        case CONTACT_GET_SUCCESS:
            return {
                loading: false,
                contact: action.payload.data
            }

        case CONTACT_GET_FAIL:
            return {
                loading: false,
                error: action.payload,
            }
        
        default:
            return state
    }
}
