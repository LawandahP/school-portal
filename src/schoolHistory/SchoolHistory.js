import React from 'react'
import { Row, Col, Image, Container } from 'react-bootstrap'

function SchoolHistory() {
    return (
        <div>
            <section className="about-us" id="about-us">
                
                <Container>
                    <h2>About Us</h2>
                    <Row>
                        <Col md={6} className="mb-3">
                            {/* <Image src="https://media.istockphoto.com/photos/school-corridor-with-lockers-3d-illustration-picture-id1267107338?b=1&k=20&m=1267107338&s=170667a&w=0&h=XQMnVJACk1ojL9HyyyNVqX1R6mq81VDe1iOOKNVtohA=" fluid></Image> */}
                        </Col>
                        <Col md={6}>
                            SKOOLY IS A MIXED BOARDING & DAY SCHOOL.
                            LOCATED ALONG NGONG ROAD IN KINOO.

                            <p>
                                Our philosophy is based on the needs and
                                characteristics of our students and staff. 
                                We focus on meeting the special needs and
                                interests of all students. Along with a solid
                                academic background, there are several
                                important traits which we hope our students
                                will develop. These include a love of learning,
                                a positive self-image and a sense of personal
                                responsibility. 
                            </p>


                        </Col>
                    </Row>

                    <Row>
                        <Col md={6} >
                        <p>
                            We hope that in all ways, we as a
                            school, will be characterized by cooperation,
                            caring and an open sharing of ideas and
                            feelings. Always the  assumption in our 
                            concept at Skooly is that each student is
                            special and that all students are capable of
                            learning.

                            It is important that our students examine
                            and appreciate their own characters,
                            that they be open and  sensitive to the
                            relationships with others and that they
                            learn to demonstrate an active concern
                            for their neighborhoods and for the world at large.
                        </p>
                        </Col>

                        <Col md={6}>
                            <Image className="image2-about" src="https://media.istockphoto.com/photos/school-corridor-with-lockers-3d-illustration-picture-id1267107338?b=1&k=20&m=1267107338&s=170667a&w=0&h=XQMnVJACk1ojL9HyyyNVqX1R6mq81VDe1iOOKNVtohA=" fluid></Image>
                        </Col>
                    </Row>
                </Container>

            </section>          
        </div>
    )
}

export default SchoolHistory
