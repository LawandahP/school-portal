import { 
    HISTORY_GET_REQUEST,
    HISTORY_GET_SUCCESS,
    HISTORY_GET_FAIL
 } from './historyConstants'


 export const historyListReducer = (state = { history: [] }, action) => {
    switch(action.type) {
        case HISTORY_GET_REQUEST:
            return {
                loading: true,
                history: []
            }
        
        case HISTORY_GET_SUCCESS:
            return {
                loading: false,
                history: action.payload.data
            }

        case HISTORY_GET_FAIL:
            return {
                loading: false,
                error: action.payload,
            }
        
        default:
            return state
    }
}
