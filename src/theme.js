import React from 'react';

import { createTheme } from '@mui/material/styles';
import { grey } from '@mui/material/colors';

const theme = createTheme({
    palette: {
        primary: {
            main: grey[50],
            light: grey[20],
        },
        secondary: {}
    } 
});

export default theme;
