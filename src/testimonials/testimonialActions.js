import axios from 'axios'

import { 
    TESTIMONIAL_GET_REQUEST,
    TESTIMONIAL_GET_SUCCESS,
    TESTIMONIAL_GET_FAIL,

    TESTIMONIAL_IMAGE_GET_REQUEST,
    TESTIMONIAL_IMAGE_GET_SUCCESS,
    TESTIMONIAL_IMAGE_GET_FAIL

} from './TestimonialConstants'

const proxy = process.env.REACT_APP_SAMIS_BACKEND_HOST

export const listTestimonials = () => async (dispatch) => {
    try {
        dispatch({
            type: TESTIMONIAL_GET_REQUEST
        })

        const { data } = await axios.get(`${proxy}/testimonials/v1`)

        dispatch({
            type: TESTIMONIAL_GET_SUCCESS,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: TESTIMONIAL_GET_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}
// {{baseUrl}}/testimonials/v1/images/download?name=bol.jpg


export const testimonialImageGet = (name) => async (dispatch) => {
    try {
        dispatch({ type: TESTIMONIAL_IMAGE_GET_REQUEST })
        const { data } = await axios.get(`${proxy}/testimonials/v1/images/download?name=${name}`) //proxy in package.json "http://127.0.0.1:8000/"

        dispatch({
            type:TESTIMONIAL_IMAGE_GET_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:TESTIMONIAL_IMAGE_GET_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}