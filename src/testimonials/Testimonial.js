import React, { useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import Slider from "react-slick";
import { Image, Container, Row, Col } from 'react-bootstrap';

// import Notification from '../components/Notification'
import Loader from '../components/Loader'

import { listTestimonials, testimonialImageGet } from './testimonialActions'
import { useTranslation } from "react-i18next";

// import testimonials from '../testimonials'

function Testimonial() {
    
    const dispatch = useDispatch(); //import listTestimonials action and use it to fetch products from serve
    const testimonialList = useSelector(state => state.testimonialList);
    const { error, loading, testimonials } = testimonialList;

    const testimonialImage = useSelector(state => state.testimonialImage)
    const { error: errorImage, loading: loadingImage, image } = testimonialImage;


    
    const { t } = useTranslation();

    // 
    

    const settings = {
        // backgroundColor: "#fff",
        className: "center",
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 2,
        autoplay: true,
        autoplaySpeed: 5000,
        pauseOnHover: true,
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                // initialSlide: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                Arrow: false,
              }
            }
          ]
        };
        
        
        useEffect(() => {
            dispatch(testimonialImageGet('bol.jpg'))
            dispatch(listTestimonials())
        }, [dispatch])

    
    
    return (

        <div>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                <path fill="#E9E9E9" fill-opacity="1" d="M0,32L40,58.7C80,85,160,139,240,176C320,213,400,235,480,218.7C560,203,640,149,720,144C800,139,880,181,960,197.3C1040,213,1120,203,1200,170.7C1280,139,1360,85,1400,58.7L1440,32L1440,320L1400,320C1360,320,1280,320,1200,320C1120,320,1040,320,960,320C880,320,800,320,720,320C640,320,560,320,480,320C400,320,320,320,240,320C160,320,80,320,40,320L0,320Z"></path>
            </svg>
                
            { loading ? <div className="my-6"><Loader /></div> 
                : 
                error ? ""
                : (
                    <div className="testimonies">
                        <Container>
                            <Row>
                                <Col>
                                    <h2>{t('heading')}</h2>
                                    <p>Don’t take our word for it, you can see below what our parents and staff
                                        have to say about our school
                                    </p>
                                </Col>
                            </Row>
                            <Slider {...settings} classNme="testimonies">
                            
                            {/* {image.name} */}
                                {testimonials.map(testimonial => ( 
                                    <div key={testimonials.id}>
                                        <Image className="testimonial-image mr-4" style={{ display: "inline-block", borderRadius: "50%", width: "200px", height: "auto" }} src={image.url} fluid/> 
                                        <p>{testimonial.info}</p>
                                        <p>{testimonial.name}</p>
                                        <p>{testimonial.role}</p>
                            
                                    </div>
                                ))}
                                
                            </Slider>
                        </Container>
                    </div>
                )
            }
        </div>
    )
}

export default Testimonial

