import {

    TESTIMONIAL_GET_REQUEST,
    TESTIMONIAL_GET_SUCCESS,
    TESTIMONIAL_GET_FAIL,

    TESTIMONIAL_IMAGE_GET_REQUEST,
    TESTIMONIAL_IMAGE_GET_SUCCESS,
    TESTIMONIAL_IMAGE_GET_FAIL

} from './TestimonialConstants'


export const testimonialListReducer = (state = { testimonials:[] }, action) =>{
    switch(action.type) {
        case TESTIMONIAL_GET_REQUEST:
            return {loading: true, testimonials:[]}
        
        case TESTIMONIAL_GET_SUCCESS:
            return {
                        loading: false,
                        testimonials: action.payload.data.items,
                        // page:action.payload.page,
                        // pages:action.payload.pages
                    }
        
        case TESTIMONIAL_GET_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}

export const testimonialImageGetReducer = (state = { image: { } }, action) =>{
    switch(action.type) {
        case TESTIMONIAL_IMAGE_GET_REQUEST:
            return {loading: true, ...state}
        
        case TESTIMONIAL_IMAGE_GET_SUCCESS:
            return {loading: false, image: action.payload.data}
        
        case TESTIMONIAL_IMAGE_GET_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}
