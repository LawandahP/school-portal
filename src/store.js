import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'

import { testimonialImageGetReducer, testimonialListReducer } from './testimonials/testimonialReducer';
import { contactGetReducer } from './contacts/contactReducer'
import { historyListReducer } from './schoolHistory/historyReducer'




const reducers = combineReducers({
    testimonialList: testimonialListReducer,
    contactList: contactGetReducer,
    historyList: historyListReducer,
    testimonialImage: testimonialImageGetReducer,
})

const initialState = {}
// const userInfoFromStorage = localStorage.getItem('userInfo') ?
//      JSON.parse(localStorage.getItem('userInfo')) : null

const middleware = [thunk]

const store = createStore(reducers, initialState,
    composeWithDevTools(applyMiddleware(...middleware)));

export default store;